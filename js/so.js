// opens listing when anchor is used
// source: https://stackoverflow.com/questions/37033406/automatically-open-details-element-on-id-call/37033774#37033774
function openTarget() {
    var hash = location.hash.substring(1);
    if(hash) var details = document.getElementById(hash);
    if(details && details.tagName.toLowerCase() === 'details') details.open = true;
  }
  window.addEventListener('hashchange', openTarget);
  openTarget();