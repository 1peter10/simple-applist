import csv
from html import escape
import urllib.request
import dominate
import datetime
import sys
import io
import json

from dominate.tags import *
from dominate.util import raw

def get_current_date():
    time = datetime.datetime.now()
    return time.strftime("%G-%m-%d")
    

def read_csv(path, column_names):
    with open(path, newline='') as f:
        # why newline='': see footnote at the end of https://docs.python.org/3/library/csv.html
        reader = csv.reader(f)
        for row in reader:
            if reader.line_num == 1:
                continue # skips the first row
            record = {name: value for name, value in zip(column_names, row)}
            yield record


def sort_by_name(e):
    return e['name']

def is_link(str):
    if str.startswith( 'http' , 0 , 4 ):
        return True

def write_page(doc,filename):
    with open(filename, 'w') as file:
        file.write(doc.render())
    return file

def write_markdown(doc,filename):
    with open(filename, 'w') as file:
        file.write(doc)
    return file

     
# TODO def generate_individual_markdown_files():
# use TOML module for this  https://pypi.org/project/toml/     


# TODO def create_search_index(records): 
# JSON index, http://elasticlunr.com/
# https://github.com/joe-crawford/Static-Site-Search
# https://pypi.org/project/jsons/
# https://docs.python.org/3/library/json.html
# Content that has to be in the index: summary description notice framework category - link to app name or (later) individual pages
def list_facts(records):
    lines=[]
    lines.append('## Quick facts\nWe currently list '+ str(len(records)) +' apps.\n\n' )
    return ''.join(lines)
    # TODO Short facts: Add number of apps, latest added apps, latest updated listings (as functions, to enable flexible reuse e.g. for landing page 



def generate_markdown_list(title,description):
    lines = []
    # Frontmatter
    lines.append('+++\ntitle = "'+title+'"\ndescription = "'+description+'"\ndate = "'+ get_current_date()+'"\ntemplate = "lists/staticlist.html"\n+++\n')
    lines.append('\n<mark>This is an experimental new form to display the contents of the LINMOBapps app list. It is still a work in progress. If you prefer the old JavaScript-heavy listing, <a href="../apps-classic/">click here</a>.</mark>\n\n')
    lines.append(list_facts(records))
    lines.append(html_list(records))
    page = ''.join(lines)
    file = write_markdown(page,"apps.md")


def create_page(records):
    doc = dominate.document(title='Experimenting with apps.csv and Python')

    doc.set_attribute('lang','en')
    
    with doc.head:
        #link(rel='stylesheet',href='css/pico.pico.classless.min.css')
        link(rel='stylesheet',href='css/sakura.css')
        #link(rel='stylesheet', href='css/almond.lite.min.css')
        link(rel='stylesheet', href='css/applist.css')
        script(type='text/javascript', src='js/so.js')
    with doc:
        with header():
            with hgroup():
                h1('Hello, Applist!')
                p('This is an attempt represent linmobapps.frama.io in a way that works better on less performant hardware.')          
                
        with main():
            with section(cls="applist"):
                with ul(cls="titles"):
                    for i in ['name', 'category', 'mobile fit', 'framework']:
                        li(strong(i.upper()))
                raw(html_list(records))

        with footer():
            raw('<p>This list is reusing <a href="https://linmobapps.frama.io/apps.csv">apps.csv</a>. It is licensed under CC BY-SA 4.0 International.</p><p><a href="https://framagit.org/1peter10/simple-applist">(Terrible) Source code</a></p><p><a href="https://framagit.org/1peter10/simple-applist/-/issues">To Do list</a></p>')
    file = write_page(doc,"applist.html")

# TODO def html_list_by_category(records):

def html_list(records):
    # records is expected to be a list of dicts
    column_names = []
    # first detect all posible keys (field names) that are present in records
    for record in records:
        for name in record.keys():
            if name not in column_names:
                column_names.append(name)
    # create the HTML line by line
    lines = []
    later = {}
    for record in records:
        for name in column_names:
            # put everything left of framework that's not supposed to be in the unopened listing in here
            if name == "repository" or name == "website" or name == "bugtracker" or name == "donations" or name == "translations" or name == "more_information" or name == "license" or name == "metadata_license" or name == "summary" or name == "summary_source" or name == "description" or name == "description_source" or name == "screenshots" or name == "status" or name == "notice":
                later_value = record.get(name, '')
                if len(later_value) > 0:
                    later[name] = later_value
            elif name == "name":
                value = record.get(name, '')
                lines.append('  <details id="' + value.lower().replace(" ","-") + '">\n    <summary>\n      <span class="name">{}</span>\n'.format(escape(value)))
            elif name == "category":
                value = record.get(name, '')
                lines.append('      <span class="category">{}</span>\n'.format(escape(value)))
            elif name == "mobile_compatibility":
                value = record.get(name, '')
                lines.append('      <span class="mobile_compatibility">{}</span>\n'.format(escape(value)))
            elif name == "repology":
                value = record.get(name, '')
                if len(value) > 0:
                    lines.append('        <li><span class="repology">Packaging status:</span>') 
                    repology_list = value.split()
                    for listing in repology_list:
                        lines.append('            <a href="https://repology.org/project/' + listing + '/versions" target="_blank">' + listing +'</a>'.format(escape(listing)))
                    lines.append('         <small>(Click to check packaging status on repology.org)</small></li>\n')
            elif name == "appstream_link":
                value = record.get(name, '')
                lines.append('        <li><span class="' + name + '">Install link:</span> <a href="{}">Attempt installation</a> <small>(This should open GNOME Software or Discover and allow you to install this app if it is available for your distribution)</small></li>\n'.format(escape(value)))
            elif name == "flatpak" or name == "metainfo_url":
                value = record.get(name, '')
                if len(name) > 0 and is_link(value) == True:
                    lines.append('        <li><span class="' + name + '">' + name.replace("_", " ").capitalize() + ':</span> <a href="'+ value +'" target="_blank">'+ value[:55] +'...</a></li>\n')
            elif name == "framework":
                value = record.get(name, '')
                lines.append('      <span class="framework">{}</span>\n    </summary>\n    <ul>\n'.format(escape(value)))
                for n in later:
                    if n == "summary" or n == "description":
                        later_value = record.get(n, '')
                        if len(later_value) > 0:
                            lines.append('        <li><span class="' + n + '">' + n.capitalize() + ':</span> {}'.format(escape(later_value)))
                    elif n == "summary_source" or n == "description_source":
                        later_value = record.get(n, '')
                        if len(later_value) > 0 and is_link(later_value) == True:
                           lines.append('    <small><a href="{}" target="_blank">Source</a></small></li>\n'.format(escape(later_value)))
                    elif n == "notice":
                        later_value = record.get(n, '')
                        if len(later_value) > 0:
                            lines.append('        <li><span class="' + n + '">' + n.capitalize() + ':</span> ')
                            list_of_notice_content = later_value.split()
                            for word in list_of_notice_content:
                                if is_link(word):
                                    lines.append('<a href="'+word+'" target="_blank">'+word+'</a> ')
                                else:
                                    lines.append(word+' ')
                            lines.append('</li>\n')
                    elif n == "repository" or n == "website" or n == "bugtracker" or n == "donations" or n == "translations" or n == "more_information" or n == "screenshots":
                        later_value = record.get(n, '')
                        list_of_later_links = later_value.split()
                        if len(list_of_later_links) >= 2:
                            lines.append('        <li><span class="' + n + '">' + n.replace("_", " ").capitalize() + ':</span>\n          <ul>')
                            for later_links in list_of_later_links:
                                lines.append('            <li><a href="'+ later_links +'" target="_blank">'+ later_links +'</a></li>\n')
                            lines.append('          </ul>\n       </li>')
                        elif len(list_of_later_links) == 1:
                            lines.append('        <li><span class="' + n +'">' + n.replace("_", " ").capitalize() + ':</span> <a href="'+ list_of_later_links[0] +'" target="_blank">' + list_of_later_links[0] + '</a></li>')
                    else: 
                        later_value = record.get(n, '')
                        if len(later_value) > 0:
                            lines.append('        <li><span class="' + n + '">' + n.capitalize() + ':</span> {}</li>\n'.format(escape(later_value)))
            else:
                value = record.get(name, '')
                if len(value) > 0:
                    lines.append('        <li><span class="' + name + '">' + name.capitalize().replace("_", " ") + ':</span> {}</li>\n'.format(escape(value)))
        lines.append('    </ul>\n  </details>\n')
    # join the lines to a single string and return it
    return ''.join(lines)
#                                          1     2          3          4             5       6        7          8           9                10               11            12              13        14       15    16      17         18     19       20    21      22         23      24                     25              26     27           28     29
records = list(read_csv('apps.csv', 'name repository website bugtracker donations translations more_information license metadata_license category summary summary_source description description_source screenshots mobile_compatibility status notice framework backend service appid scale-to-fit flatpak repology aur postmarketos debian appstream_link metainfo_url freedesktop_categories languages buildsystem date reporter updated updated_by'.split()))
# Can I make these things strings?
#records = list(read_csv('apps.csv', '"Name" "Repository" "Website" "More Information" "License" category summary summary_source description description_source screenshots mobile_compatibility framework backend service appid scale-to-fit notice flatpak repology aur postmarketos debian appstream_link metainfo_url freedesktopcategories languages buildsystem reporter date'.split()))
records.sort(key=sort_by_name)

create_page(records)


def get_categories(records):
    column_names=[]
    categories=[]

    for record in records:
        for name in record.keys():
            if name not in column_names:
                column_names.append(name)
    categories=[]
    for record in records:
         for name in column_names:
            if name == "category":
                value = record.get(name, '')
                categories.append(value)
                categories = list(dict.fromkeys(categories))
                # = categories.sort()
                return categories
                # print(len(categories))
                
get_categories(records)
# Records for one category 
# records = [r for r in records if r['category'] == 'social media']
#app_categories = records(data.value())

# Todo: See how data can be reordered without changing the entire csv
# Todo: reorder alphabetical by name to make this like the old list
generate_markdown_list("Apps","A list of apps that work on Linux Phones like the PinePhone or Librem 5.")
# print(html_list(records))